<?php 
//echo"hellooooo";
$filename2="paintings.txt";
$fp2 = @fopen($filename2, 'r');

if ($fp2) {
  $read = explode("\n", fread($fp2, filesize($filename2)));
}

$array = array();

//162
for ($i = 0; $i < 161; $i++){
  $arrayi = split("~", $read[$i]);
  $array[$i] = $arrayi;
 // echo $arrayi[0] . "<br>";
  //echo $array[0][0] . "<br>";
}

/*for ($i = 5; $i < 6; $i++){
  for ($j = 0; $j < sizeof($array[0]); $j++){
      echo $array[$i][$j] . "</br>";
  }
}*/

?>

<!--BEGIN THE HTML-->
<!DOCTYPE html>
<html lang="en">
  <!--add all the dependencies-->
  <head>
    <title>SE3316 Lab3</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>  
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Handwriting">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="regular.css" rel="stylesheet"> 
   <!--add the handriwitng style as specified in the assignment description-->
 <style>
      #header {
        font-family: 'Handwriting', serif;
        font-size: 48px;
      }
    </style>

 </head>
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">LAB 3</a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="work.php">Work</a></li>  
                <li><a href="artists.php">Artists</a></li>                
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>
    
<!--CAROUSEL!!!-->
    <div id="Carousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#Carousel" data-slide-to="0" class="active"></li>
        <li data-target="#Carousel" data-slide-to="1"></li>
        <li data-target="#Carousel" data-slide-to="2"></li>
        <li data-target="#Carousel" data-slide-to="3"></li>
        <li data-target="#Carousel" data-slide-to="4"></li>
      </ol>
     <div class="carousel-inner">

<?php
    for ($i = 0; $i < 5; $i++){
      if ($i==0){
        echo " <div class='item active'> 
          <img src=\"art-images/paintings/medium/" . $array[$i][3] . ".jpg\" alt=\"". $array[$i][4] ."\" title=\"" . $array[$i][4] . "\" style='height: Auto;width:  100%;'/> 
            <div class='container'>   
              <div class='carousel-caption'>
                <h1>" . $array[$i][4] . "</h1>
                <p>" . $array[$i][6] . "</p>
                <p><a class='btn btn-lg btn-primary' href='" . $array[$i][12] . "' role='button'>Learn more</a></p>
              </div>
          </div>
        </div>";
      }
      else{
        echo "
            <div class='item'>
            <img src=\"art-images/paintings/medium/" . $array[$i][3]. ".jpg\" alt=\"" . $array[$i][4] ."\" title=\"" . $array[$i][4] . "\" style='height: Auto;width:  100%;'/>
            <div class='container'>
              <div class='carousel-caption'>
                <h1>" . $array[$i][4] . "</h1>
                <p>" . $array[$i][6] . "</p>
                <p><a class='btn btn-lg btn-primary' href='" . $array[$i][12] . "' role='button'>Learn more</a></p>
              </div>
            </div>
          </div>";
      }
    }
?>

    </div>
      <a class="left carousel-control" href="#Carousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#Carousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div> 

 <div class="container marketing">

      <div class="row">
        
        <?php
         
        for($i=5; $i<8;$i++){
         $str = substr($array[$i][5], 0, 140) . "...";


          echo " <div class=\"col-lg-4\">
          <img class='img-circle' src='art-images/paintings/medium/" . $array[$i][3] . ".jpg' alt='Portrait of Dora Maar' title='Portrait of Dora Maar' style='width:100px; height:100px;' />
         <h2>" . $array[$i][4] . "</h2>
          <p class=\"text-justify\">" . $str . "</p>
          <p><a class=\"btn btn-default\" href='" . $array[$i][12] . "' role=\"button\">View details &raquo;</a></p>
        </div>";
        }
         echo"</div>
         <div class=\"row\">";

        for($i=8; $i<11;$i++){
           $str = substr($array[$i][5], 0, 140) . "...";
          echo " <div class=\"col-lg-4\">
          <img class='img-circle' src='art-images/paintings/medium/" . $array[$i][3] . ".jpg' alt='Portrait of Dora Maar' title='Portrait of Dora Maar' style='width:100px; height:100px;' />
         <h2>" . $array[$i][4] . "</h2>
          <p class=\"text-justify\">" . $str . "</p>
          <p><a class=\"btn btn-default\" href='" . $array[$i][12] . "' role=\"button\">View details &raquo;</a></p>
        </div>";
        }
        echo "</div>";


        ?>
      </body>
</html>


