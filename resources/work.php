
<?php 
$filename2="paintings.txt";
$fp2 = @fopen($filename2, 'r');

if ($fp2) {
  $read = explode("\n", fread($fp2, filesize($filename2)));
}

$array = array();

//162
for ($i = 0; $i < 161; $i++){
  $arrayi = split("~", $read[$i]);
  $array[$i] = $arrayi;
}

/*for ($i = 0; $i < 1; $i++){
  for ($j = 0; $j < sizeof($array[0]); $j++){
      echo $array[$i][$j] . "</br>";
  }
}*/
?>

<!DOCTYPE html>
<html lang="en">
  <head>
   
   <meta http-equiv="Content-Type" content="text/html;charset=us-ansi">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Lab3 SE3316A</title>
    
<link href='https://fonts.googleapis.com/css?family=Cuprum|Cookie' rel='stylesheet' type='text/css'>  
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="regular.css" rel="stylesheet">
    <link href="lab3.css" rel="stylesheet">
  </head>
  
   <body>
  <header>

   <div id="topHeaderRow">
      <div class="container">
         <nav role="navigation" class="navbar navbar-inverse ">
            <div class="navbar-header">
               <button data-target=".navbar-ex1-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
               <p class="navbar-text">Welcome to <strong>Art Store</strong>, <a class="navbar-link" href="#">Login</a> or <a class="navbar-link" href="#">Create new account</a></p>
            </div>

            <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
               <ul class="nav navbar-nav">
                  <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a></li>                  
               </ul>
            </div>  
         </nav> 
      </div>  
   
   <div id="logoRow">
      <div class="container">
         <div class="row">
            <div class="col-md-8">
                <h1>Art Store</h1> 
            </div>
            
            <div class="col-md-4">
               <form role="search" class="form-inline">
                  <div class="input-group">
                     <label for="search" class="sr-only">Search</label>
                     <input type="text" name="search" placeholder="Search" class="form-control">
                     <span class="input-group-btn">
                     <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                     </span>
                  </div>
               </form> 
            </div>   
         </div>          
      </div>  
   </div>   
   
   <div id="mainNavigationRow">
      <div class="container">

         <nav role="navigation" class="navbar navbar-default">
            <div class="navbar-header">
               <button data-target=".navbar-ex1-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
            </div>

            <div class="collapse navbar-collapse navbar-ex1-collapse">
             <ul class="nav navbar-nav">
               <li><a href="index.php">Home</a></li>
               <li><a href="about.php">About Us</a></li>
               <li class="active"><a href="#">Art Works</a></li>
               <li><a href="artists.php">Artists</a></li>
               <li class="dropdown">
                 <a data-toggle="dropdown" class="dropdown-toggle" href="#">Specials <b class="caret"></b></a>
                 <ul class="dropdown-menu">
                   <li><a href="#">Special 1</a></li>
                   <li><a href="#">Special 2</a></li>                   
                 </ul>
               </li>
             </ul>              
            </div>
         </nav> 
      </div>  
   </div>  
   
</header>

  <div class="container">

   <div class="row">

<div id="Carousel" class="carousel slide" data-ride="carousel">

  <div class="carousel-inner">

<?php

  for ($i = 0; $i < 10; $i++){
      if ($i==0){

          echo"
    <div class='item active'>
        <div class=\"container\">
        <div class=\"col-md-10\">
         <h2>" . $array[$i][4] . "</h2>
         <p><a href=\"#\">" . $array[$i][6] . "</a></p>
         <div class=\"row\">
            <div class=\"col-md-5\"><img class=\"img-thumbnail img-responsive\" src=\"art-images/paintings/medium/" . $array[$i][3] . ".jpg\" title=\"" . $array[$i][4] . "\"></div>
            <div class=\"col-md-7\">
               <p style=\"text-align: justify;   text-justify: inter-word;\">" . $array[$i][5] . "</p>
               <p class=\"price\">" . $array[$i][11] . "</p>
               <div class=\"btn-group btn-group-lg\">
                 <button class=\"btn btn-default\" type=\"button\">
                     <a href=\"#\"><span class=\"glyphicon glyphicon-gift\"></span> Add to Wish List</a>  
                 </button>
                 <button class=\"btn btn-default\" type=\"button\">
                  <a href=\"#\"><span class=\"glyphicon glyphicon-shopping-cart\"></span> Add to Shopping Cart</a>
                 </button>
               </div>               
               <p>&nbsp;</p>
               <div class=\"panel panel-default\">
                 <div class=\"panel-heading\">Product Details</div>
                 <table class=\"table\">
                   <tbody><tr>
                     <th>Date:</th>
                     <td>" . $array[$i][6] . "</td>
                   </tr>
                   <tr>
                     <th>Medium:</th>
                     <td>" . $array[$i][9] . "</td>
                   </tr>  
                   <tr>
                     <th>Dimensions:</th>
                     <td>" . $array[$i][7] . "cm x " .  $array[$i][8] . "cm</td>
                   </tr> 
                   <tr>
                     <th>Home:</th>
                     <td><a href=\"#\">" .  $array[$i][10] . "</a></td>
                   </tr>  
                   <tr>
                     <th>Link:</th>
                     <td><a href=\"" .  $array[$i][12] . "\">Wiki</a></td>
                   </tr>     
                 </tbody></table>
               </div>                              
            </div>  
         </div>
         </div>
         </div>
         </div>";
      }
      else{
        echo"
      <div class='item'>
        <div class=\"container\">
        <div class=\"col-md-10\">
         <h2>" . $array[$i][4] . "</h2>
         <p><a href=\"#\">" . $array[$i][6] . "</a></p>
         <div class=\"row\">
            <div class=\"col-md-5\"><img class=\"img-thumbnail img-responsive\" src=\"art-images/paintings/medium/" . $array[$i][3] . ".jpg\" title=\"" . $array[$i][4] . "\"></div>
            <div class=\"col-md-7\">
               <p style=\"text-align: justify;   text-justify: inter-word;\">" . $array[$i][5] . "</p>
               <p class=\"price\">" . $array[$i][11] . "</p>
               <div class=\"btn-group btn-group-lg\">
                 <button class=\"btn btn-default\" type=\"button\">
                     <a href=\"#\"><span class=\"glyphicon glyphicon-gift\"></span> Add to Wish List</a>  
                 </button>
                 <button class=\"btn btn-default\" type=\"button\">
                  <a href=\"#\"><span class=\"glyphicon glyphicon-shopping-cart\"></span> Add to Shopping Cart</a>
                 </button>
               </div>               
               <p>&nbsp;</p>
               <div class=\"panel panel-default\">
                 <div class=\"panel-heading\">Product Details</div>
                 <table class=\"table\">
                   <tbody><tr>
                     <th>Date:</th>
                     <td>" . $array[$i][6] . "</td>
                   </tr>
                   <tr>
                     <th>Medium:</th>
                     <td>" . $array[$i][9] . "</td>
                   </tr>  
                   <tr>
                     <th>Dimensions:</th>
                     <td>" . $array[$i][7] . "cm x " .  $array[$i][8] . "cm</td>
                   </tr> 
                   <tr>
                     <th>Home:</th>
                     <td><a href=\"#\">" .  $array[$i][10] . "</a></td>
                   </tr>  
                   <tr>
                     <th>Link:</th>
                     <td><a href=\"" .  $array[$i][12] . "\">Wiki</a></td>
                   </tr>     
                 </tbody></table>
               </div>                              
            </div>  
         </div>
         </div>
         </div>
         </div>";
      }
  }
?>
    </div>
    <a class="left carousel-control" href="#Carousel" role="button" data-slide="prev" id="carL"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#Carousel" role="button" data-slide="next" id="carR"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>


    </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
