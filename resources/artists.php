<?php
$filename2="artists.txt";
$fp2 = @fopen($filename2, 'r');

if ($fp2) {
  $read = explode("\n", fread($fp2, filesize($filename2)));
}

$array = array();

//162
for ($i = 0; $i < 25; $i++){
  $arrayi = split("~", $read[$i]);
  $array[$i] = $arrayi;
}

/*for ($i = 0; $i < 1; $i++){
  for ($j = 0; $j < sizeof($array[0]); $j++){
      echo $array[$i][$j] . "</br>";
  }
}*/
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>SE3316 Lab3</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="artists.css" rel="stylesheet">
</head>
  <body>
      
  <header>

   <div>
      <div class="container">
         <nav class="navbar navbar-inverse " role="navigation">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
               <p class="navbar-text">Welcome to <strong>Art Store</strong>, <a href="#" class="navbar-link">Login</a> or <a href="#" class="navbar-link">Create new account</a></p>
            </div>

            <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
               <ul class="nav navbar-nav">
                  <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a></li>                  
               </ul>
            </div>  
         </nav>  
      </div> 
   </div>  
   
   <div>
      <div class="container">
         <div class="row">
            <div class="col-md-8">
                <h1>Art Store</h1> 
            </div>
            
            <div class="col-md-4">
               <form class="form-inline" role="search">
                  <div class="input-group">
                     <label class="sr-only" for="search">Search</label>
                     <input type="text" class="form-control" placeholder="Search" name="search">
                     <span class="input-group-btn">
                     <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                     </span>
                  </div>
               </form> 
            </div>  
         </div>         
      </div> 
   </div>  
   
   <div>
      <div class="container">
         <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
            </div>

            <div class="collapse navbar-collapse navbar-ex1-collapse">
             <ul class="nav navbar-nav">
               <li><a href="index.php">Home</a></li>
               <li ><a href="about.php">About Us</a></li>
               <li><a href="work.php">Art Works</a></li>
               <li class="active"><a href="#">Artists</a></li>
               <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Specials <b class="caret"></b></a>
                 <ul class="dropdown-menu">
                   <li><a href="#">Special 1</a></li>
                   <li><a href="#">Special 2</a></li>                   
                 </ul>
               </li>
             </ul>              
            </div>
         </nav>  
      </div> 
   </div>  
  </header>

<div class="container">
<h2>This Week's Best Artists</h2>
   <div class="alert alert-warning" role="alert">Each week, best artists
   </div>   
      <div class="row">
       <div id="Carousel" class="carousel slide" data-ride="carousel">
           <div class="carousel-inner">
              <div class='item active'>
                <div class='container'>
        

<?php


for($i=0; $i < 6; $i++){
  echo "<div class='col-md-2'>
                    <div class='thumbnail'><img src='art-images/artists/medium/" . $array[$i][0] . ".jpg' style=\"width:175px; height:175px;\"/>
                     <br/>
                      <div class='caption'>
                      <h4>" . $array[$i][1] . " "  . $array[$i][2] . "</h4>
                      <p><a class='btn btn-info' role='button' href='" . $array[$i][7] . "' role='button'>Learn more</a></p>
                    </div>
                  </div>
                </div>";

  if ($i == 5){
    echo "</div></div>";
  }
}

echo "<div class='item'><div class='container'>";

for($i=6; $i < 12; $i++){
  echo "<div class='col-md-2'>
                    <div class='thumbnail'><img src='art-images/artists/medium/" . $array[$i][0] . ".jpg' style=\"width:175px; height:175px;\"/>
                     <br/>
                      <div class='caption'>
                      <h4>" . $array[$i][1] . " "  . $array[$i][2] . "</h4>
                      <p><a class='btn btn-info' role='button' href='" . $array[$i][7] . "' role='button'>Learn more</a></p>
                    </div>
                  </div>
                </div>";

  /*if ($i == 5){
   echo "</div></div>";
  }*/
}

?>
          </div>
        </div>       
        </div>
        <a class="left carousel-control" href="#Carousel" role="button" data-slide="prev" id="carL"><span class="glyphicon glyphicon-chevron-left" ></span></a>
      <a class="right carousel-control" href="#Carousel" role="button" data-slide="next" id="carR"><span class="glyphicon glyphicon-chevron-right" ></span></a>
   
       </div>
      </div>





<div class="row">
       <div id="Carousel2" class="carousel slide" data-ride="carousel">
           <div class="carousel-inner">
              <div class='item active'>
                <div class='container'>
        

<?php


for($i=12; $i < 18; $i++){
  echo "<div class='col-md-2'>
                    <div class='thumbnail'><img src='art-images/artists/medium/" . $array[$i][0] . ".jpg' style=\"width:175px; height:175px;\"/>
                     <br/>
                      <div class='caption'>
                      <h4>" . $array[$i][1] . " "  . $array[$i][2] . "</h4>
                      <p><a class='btn btn-info' role='button' href='" . $array[$i][7] . "' role='button'>Learn more</a></p>
                    </div>
                  </div>
                </div>";

  if ($i == 17){
    echo "</div></div>";
  }
}

echo "<div class='item'><div class='container'>";

for($i=18; $i < 24; $i++){
  echo "<div class='col-md-2'>
                    <div class='thumbnail'><img src='art-images/artists/medium/" . $array[$i][0] . ".jpg' style=\"width:175px; height:175px;\"/>
                     <br/>
                      <div class='caption'>
                      <h4>" . $array[$i][1] . " "  . $array[$i][2] . "</h4>
                      <p><a class='btn btn-info' role='button' href='" . $array[$i][7] . "' role='button'>Learn more</a></p>
                    </div>
                  </div>
                </div>";
}
?>
          </div>

        </div> 

        </div>
      <a class="left carousel-control" href="#Carousel2" role="button" data-slide="prev" id="carL"><span class="glyphicon glyphicon-chevron-left" ></span></a>
        <a class="right carousel-control" href="#Carousel2" role="button" data-slide="next" id="carR"><span class="glyphicon glyphicon-chevron-right" ></span></a> 
   
  <h4>Artists by Genre</h4>
   <div class="progress">
     <div class="progress-bar progress-bar-info" style="width: 15%">
       <span>Gothic</span>
     </div>
     <div class="progress-bar progress-bar-success" style="width: 19%">
       <span>Renaissance</span>
     </div>
     <div class="progress-bar progress-bar-warning" style="width: 20%">
       <span>Baroque</span>
     </div>
     <div class="progress-bar progress-bar-danger" style="width: 21%">
       <span >Pre-Modern</span>
     </div>  
     <div class="progress-bar" style="width: 25%">
       <span >Modern</span>
     </div>
   </div>

       </div>



      </div>

</body>
</html>